# LEKTUR (TEAM F React Native)

## Context and Goal
This is a project from Glints Academy Batch #9. Out team is responsible for creating a mobile app using React Native.

##Download APK (Android)
If you want to download and running the app in your android phone, here is the [link](https://drive.google.com/drive/folders/1mzzN7fVY0UYyr-_whJ1SSdTL4GPp6aIZ?usp=sharing)

## Preview

### Student's Dashboard Demo
<img src="Students-Dashboard.gif" width="300" height="500">
> Note: Might take some time to load

### Teacher's Dashboard Demo
<img src="Teacher-Dashboard.gif" width="300" height="500">
> Note: Might take some time to load

## Main technologies used
- [React Native](https://github.com/facebook/react-native)
- [Redux](http://redux.js.org/)
- [Redux-Saga](https://redux-saga.js.org/)
- [Async Storage](https://react-native-async-storage.github.io/async-storage/)

## Running the project

- Clone this project
```
git clone < project-url.git >
```
- Install the dependencies with npm install
```
npm install
```
- Open terminal and run this command and let it open
```
react-native start
```
-Open another terminal and run this command
```
react-native run-android
```
