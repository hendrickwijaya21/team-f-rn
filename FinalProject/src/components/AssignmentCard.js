import React from 'react';
import { Text, View, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import STYLES from '../styles/style'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const AssignmentCard = ({
  item,
}) => {
  const months = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  const complete_course = item.updated_at.split('T')
  const date = complete_course[0].split('-')
  const time = complete_course[1].split('').splice(0, 8).join('')
  const navigation = useNavigation()
  const result = []
  return (
    <View style={{borderColor:'#E5E5E5', borderBottomWidth: 1, padding: width* 0.05, margin: width * 0.025}}>
      <Text style={{...STYLES.nameList, fontSize: 25}}>{item.course.title}</Text>
      <Text style={{color:'blue', textDecorationLine:'underline'}}>{item.teacher.name}</Text>
      {item.totalCorrect != null && <Text style={{color:'#999'}}>Completed at: {date[2]} {months[parseInt(date[1])]} {date[0]} {time}</Text>}
      {item.totalCorrect == null && <Text style={{color:'#999'}}>Completed at: -</Text>}
      {item.totalCorrect != null && <View style={{flexDirection:'row', width: width * 0.7, alignItems: 'center'}}>
        <Text style={{...STYLES.textOrange, fontSize: height*0.035, marginRight:width*0.04}}>{item.score.toFixed(1)}%</Text>
        <Text style={{color:'#999'}}>{item.totalCorrect} / {item.totalQuestion} Question Correct</Text>
      </View>}
      {item.totalCorrect != null && <View style={{flexDirection:'row', width: width * 0.5,}}>
        <TouchableOpacity style={{...STYLES.buttonWhite, width: width*0.25}}
          onPress={()=>{
            navigation.navigate("Result",{test_id:item.test._id,
               title:item.course.title, 
               student_test_id: item.id,
               score:item.score.toFixed(1),
               totalQuestion:item.totalQuestion,
               totalCorrect: item.totalCorrect
              })
          }}
        >
          <Text style={STYLES.textOrange}>Review</Text>
        </TouchableOpacity>
      </View>}
      {item.totalCorrect == null && <View style={{flexDirection:'row', width: width * 0.5, justifyContent:'space-around', alignItems: 'center'}}>
        <TouchableOpacity style={{...STYLES.buttonWhite, width: width*0.25}}
          onPress={()=>{
            navigation.navigate("TestPage",{test_id:item.test._id, title:item.course.title, student_test_id: item.id})
          }}
        >
          <Text style={STYLES.textOrange}>Take Quiz</Text>
        </TouchableOpacity>
        <Text>No result yet</Text>
      </View>}
    </View>
  )
};

export default AssignmentCard;
