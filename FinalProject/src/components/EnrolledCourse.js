import React from 'react';
import { FlatList, Text, View, Dimensions, Image, TextInput, TouchableOpacity, } from 'react-native';
import ProgressBarClassic from 'react-native-progress-bar-classic';
import {useSelector, useDispatch} from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import STYLES from '../styles/style'
import {getLessonsCourse} from '../store/action'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const EnrolledCourse = ({
  data,
}) => {
  const token = useSelector(state => state.user.token)
  const navigation = useNavigation()
  const dispatch = useDispatch()
  console.log(data)
  return(
    <FlatList 
      data={data}
      renderItem={({item})=>{
        if(item.status == 'active'){
          return (
            <View style={{padding: width * 0.03, borderBottomWidth: 1, marginBottom: height * 0.01, borderColor:'#e9e9e9'}}>
              <Image source={{uri:item.course.image}} style={{width: width * 0.96, height: height * 0.27}}/>
              <Text style={STYLES.nameList}>{item.course.title}</Text>
              <Text style={{...STYLES.status, fontSize: height * 0.015, marginBottom: height * 0.01}}>By {item.teacher.name}</Text>
              <ProgressBarClassic progress={item.totalLessonCompleted/item.totalLesson * 100} />
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <TouchableOpacity 
                  onPress={()=>{
                    dispatch(getLessonsCourse({id: item.course.id, token: token}))
                    navigation.navigate("CourseContent", {id: item.course.id, index: item.totalLessonCompleted, title:item.course.title, poster:item.course.image})
                  }}
                >
                  <Text style={{...STYLES.status, fontSize: height * 0.017, textDecorationLine:'underline'}}>{item.totalLessonCompleted}/{item.totalLesson} Completed</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={()=>{
                    dispatch(getLessonsCourse({id: item.course.id, token: token}))
                    navigation.navigate("CourseMaterial")
                  }}
                >
                  <Text style={{...STYLES.status, fontSize: height * 0.017,textDecorationLine:'underline'}}>See course materials</Text>
                </TouchableOpacity>
              </View>
              { item.totalLessonCompleted < item.totalLesson &&
              <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width: width * 0.6, height: height * 0.05, backgroundColor:"#3E89AE", alignSelf:'center', margin: height * 0.02}}
                onPress={()=>{
                  dispatch(getLessonsCourse({id: item.course.id, token: token}))
                  navigation.navigate('CoursePlay', {id: item.course.id, index: item.totalLessonCompleted, title:item.course.title, poster:item.course.image})
                }}
              >
                <Text style={{...STYLES.textWhite, fontSize: height * 0.02}}>Lesson #{item.totalLessonCompleted + 1}</Text>
              </TouchableOpacity>}
              { item.totalLessonCompleted == item.totalLesson &&
                <Text style={{ ...STYLES.active, fontSize: height * 0.02, alignSelf:'center'}}>Take the quiz and get result Now</Text>
              }
            </View>
          )
        }
        if(item.status == 'pending'){
          return(
            <View style={{padding: width * 0.02}}>
              <Image source={{uri:item.course.image}} style={{width: width * 0.96, height: height * 0.27}}/>
              <Text style={STYLES.nameList}>{item.course.title}</Text>
              <Text style={{...STYLES.status, fontSize: height * 0.015, marginBottom: height * 0.01}}>By {item.teacher.name}</Text>
              <View style={{justifyContent:'center', alignItems:'center'}}>
                <Text style={{...STYLES.status, fontSize: height * 0.02, marginBottom: height * 0.01}}>Waiting for Approval</Text>
              </View>
            </View>
          )
        }
        if(item.status == 'completed'){
          return(
            <View style={{padding: width * 0.02}}>
              <Image source={{uri:item.course.image}} style={{width: width * 0.96, height: height * 0.27}}/>
              <Text style={STYLES.nameList}>{item.course.title}</Text>
              <Text style={{...STYLES.status, fontSize: height * 0.015, marginBottom: height * 0.01}}>By {item.teacher.name}</Text>
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <TouchableOpacity 
                  onPress={()=>{
                    dispatch(getLessonsCourse({id: item.course.id, token: token}))
                    navigation.navigate("CourseContent", {id: item.course.id, index: item.totalLessonCompleted, title:item.course.title, poster:item.course.image})
                  }}
                >
                  <Text style={{...STYLES.status, fontSize: height * 0.017, textDecorationLine:'underline'}}>{item.totalLessonCompleted}/{item.totalLesson} Completed</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={()=>{
                    dispatch(getLessonsCourse({id: item.course.id, token: token}))
                    navigation.navigate("CourseMaterial")
                  }}
                >
                  <Text style={{...STYLES.status, fontSize: height * 0.017,textDecorationLine:'underline'}}>See course materials</Text>
                </TouchableOpacity>
              </View>
              <View style={{justifyContent:'center', alignItems:'center'}}>
                <Text style={{...STYLES.active, fontSize: height * 0.02, marginBottom: height * 0.01}}>You already finished the course</Text>
              </View>
            </View>
          )
        }
      }}
    />
)};

export default EnrolledCourse;
