import React from 'react';
import { Text, View, TouchableOpacity, Dimensions, FlatList } from 'react-native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const SubNavBar = ({
    navBar, target, onPressed
}) =>  {
	return (
		<View style={{backgroundColor:"rgba(240,166,62,0.2)", height:height*0.05, justifyContent: 'center'}}>
			<FlatList 
				horizontal
				rowWrapperStyle={{justifyContent: 'space-between'}}
				data={navBar}
				contentContainerStyle={{flexGrow: 1, justifyContent: 'space-around', alignItems:'center'}}
				keyExtractor={item=>item}
				renderItem={({item})=>{
					return (
						<TouchableOpacity onPress={()=>{onPressed(item)}}>
							<Text style={{fontWeight: item == target ? 'bold': null, fontSize:height*0.02}}>{item}</Text>
						</TouchableOpacity>
					)
				}}
			/>
		</View>
)};

export default SubNavBar;
