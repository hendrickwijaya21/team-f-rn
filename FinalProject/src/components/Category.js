import React from 'react';
import { Text, View, Dimensions, TouchableOpacity, FlatList } from 'react-native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

function Category({data, target, onPressed}) {
  return (
    <FlatList
			horizontal
			showsHorizontalScrollIndicator={false}
			data={data}
			keyExtractor={(item)=> item}
			renderItem={({item}) => {
				return (
          <TouchableOpacity 
            style={{
              backgroundColor: target == item ? "#16213B": '#fff',
              height:height*0.068,
              borderColor: "#16213B",
              padding: 10,
              margin: 10, 
              justifyContent:'center'
            }} 
            onPress={()=>onPressed(item)}
          >
            <Text style={{
              color: item == target ? '#fff' : '#16213B',
              letterSpacing: 3,
              fontSize: 15,
              }}
            >{item}</Text>
          </TouchableOpacity>
				)
			}}
		/>
)};

export default Category;
