import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useSelector, useDispatch} from 'react-redux'
import ProgressBarClassic from 'react-native-progress-bar-classic';

import STYLES from '../styles/style'
import {getListStudent} from '../store/action'
import authAPI from '../api/auth'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const acceptStudent = async (token, id_enroll) => {
  try{
    const res = await authAPI.patch(`/enrolls/accept/${id_enroll}`, {}, {'headers': { 'Authorization': `Bearer ${token}`}})
  } catch(e){
    console.log(e)
  }
  
}

const ListStudent = ({
  data,
}) => {
  const dispatch = useDispatch()
  const token = useSelector(state=>state.user.token)
  return (
    <View>
      <FlatList
        data ={data}
        renderItem={({item})=>{
          if(item.status == 'active'){
            return (
              <View style={STYLES.listStudent}>
                <View style={{width: width * 0.7}}>
                  <Text style={STYLES.nameList}>{item.user.name}</Text>
                  <Text style={STYLES.status}>{item.totalLessonCompleted}/{item.totalLesson} Course Complete</Text>
                  <ProgressBarClassic progress={item.totalLessonCompleted/item.totalLesson * 100} />
                </View>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <MaterialIcons name="check-circle" style={{...STYLES.active, marginRight: 10}}/>
                  <Text style={STYLES.active}>
                     Active
                  </Text>  
                </View>
              </View>
            )
          }
          if(item.status == 'pending'){
            return (
              <View style={STYLES.listStudent}>
                <Text style={STYLES.nameList}>{item.user.name}</Text>
                <View>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <MaterialIcons name="circle" style={{...STYLES.pending, marginRight: 10}}/>
                    <Text style={STYLES.pending}>Pending</Text>
                  </View>
                  <TouchableOpacity style={{...STYLES.buttonOrange, width: width *0.24}}
                    onPress={async ()=>{
                      await acceptStudent(token, item.id)
                      await dispatch(getListStudent({id: item.course, token: token}))
                    }}
                  >
                    <Text style={{...STYLES.textWhite, fontWeight: 'bold'}}>Accept</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )
          }
          if(item.status == 'completed'){
            return (
              <View style={STYLES.listStudent}>
                <View>
                  <Text style={STYLES.nameList}>{item.user.name}</Text>
                  <Text style={STYLES.score}>{item.score.toFixed(1)}%</Text>
                  <Text style={STYLES.status}>Assesment Score</Text>
                </View>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                  <MaterialIcons name="check-circle" style={{...STYLES.completed, marginRight: 10}}/>
                  <Text style={STYLES.completed}>
                     Completed
                  </Text>  
                </View>
              </View>
            )
          }
        }}
      />
    </View>
)};

export default ListStudent;
