import React from 'react';
import { Text, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import {enrollClass} from '../store/action'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const AllCourse = ({
  id, title, imageUrl, teacher, totalStudent, onClicked
}) => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const token = useSelector(state => state.user.token)
  return (
    <View style={{backgroundColor:'white', width:width*0.70, marginHorizontal:width*0.05, borderRadius: 12, overflow: 'hidden',}}>
      <TouchableOpacity onPress={()=>{
        onClicked()
        navigation.navigate('CourseDetails', {id: id})}
      }>
      <Image
      source={{uri: `${imageUrl}`}}
      style={{width: "100%", height: height*0.2}}
      />
      <Text
        style={{
          marginTop: height * 0.01,
          marginLeft: width * 0.01,
          fontSize: height*0.017,
          fontWeight: 'bold',
          color: '#3E89AE'
        }}
      >
        {totalStudent} Enrolled
      </Text>
      <Text
        style={{
          marginTop: height * 0.01,
          marginLeft: width * 0.01,
          fontSize: height*0.018,
          fontWeight: 'bold',
          color: '#000000'
        }}
      >
        {title}
      </Text>
      <Text
        style={{
          marginLeft: width * 0.01,
          fontSize: height*0.015,
          color: '#999999',
        }}>
        {teacher}
      </Text>
      </TouchableOpacity>
      <TouchableOpacity style={{
        width:width*0.25, 
        height:height*0.04,
        alignSelf:'flex-end', 
        backgroundColor:'#EF9C27',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
        margin: height*0.015
      }}
        onPress={() => {
          dispatch(enrollClass({id:id, teacher: teacher, title: title, token:token, image:imageUrl}))
        }}>
        <Text style={{
          color:'#ffffff',
          fontSize:height * 0.018}}>
          Enroll Now
        </Text>
      </TouchableOpacity> 
    </View>
)};

export default AllCourse;
