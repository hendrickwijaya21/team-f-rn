import React from 'react';
import { Text, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const CoursesCategory = ({
  id, imageUrl, teacher, title, totalVideos, totalLesson, description, category, onClicked
}) => {
  const navigation = useNavigation()
  return (
    <View style={{borderColor:'#a0a0a0', backgroundColor:'white', marginTop:20, borderWidth: 1, width: width * 0.62, marginHorizontal: 10}}>
      <TouchableOpacity onPress={()=>{
        onClicked()
        navigation.navigate('CourseDetails', {id})}
        }>
      <Image source={{uri: `${imageUrl}`}}
        style={{width: '100%', height: height*0.18}}
      />
      <Text
        style={{
          marginTop: 10,
          marginLeft: 5,
          fontSize: height*0.02,
          fontWeight: 'bold',
          color: '#000000'
        }}
      >
          {title}
        </Text>
      <Text
        style={{
          marginTop: 2,
          marginLeft: 5,
          fontSize: height*0.017,
          color: '#3E89AE'
      }}>
        by {teacher}</Text>
      <View style={{flexDirection:'row', justifyContent:'space-around'}}>
      <Text
        style={{color: '#999999', fontSize:height*0.014}}
      >
        {totalVideos} Videos</Text>
      <Text
        style={{color: '#999999',  fontSize:height*0.014}}
      >
       {totalLesson} Learning Material</Text>
       </View>
       <View style={{marginTop: height*0.015, padding: 10,}}>
       <Text ellipsizeMode='tail' numberOfLines={4}>
        {description}</Text>
       </View>
       <Text
        style={{
          fontSize: 17,
          letterSpacing: 2,
          color: '#000000',
          backgroundColor: '#FFDCDC',
          height: height * 0.05,
          textAlign: 'center',
          textAlignVertical: 'center',
        }}
      >
       {category}</Text>
       </TouchableOpacity>
    </View>
)};

export default CoursesCategory;
