import React,{useState, useEffect} from 'react';
import { Text, View, 
  TextInput, Dimensions, 
  FlatList, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useSelector, useDispatch} from 'react-redux';

import {searchCourse,
  detailCourse, contentCourse,
  requestCategoryCourse} from '../../store/action'
import SearchResult from '../../components/SearchResult';
import STYLES from '../../styles/style';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const SearchPage = ({
  navigation,
}) => {
  const searchResult = useSelector(state=>state.search)
  const [search, setSearch] = useState('')
  const dispatch = useDispatch()
  const EmptyList = () =>{
    return(
      <View style={{alignItems:'center', justifyContent:'center',width: width, height: height * 0.6}}>
        <Text style={{...STYLES.subText, fontSize:height * 0.02}}>
          Search Course...
        </Text>
      </View>
    )
  }
  return (
    <View style={{flex:1}}>
      <View style={{flexDirection:'row', width: width, alignItems:'center',flex: 1, padding: width * 0.02}}>
        <TextInput 
          placeholder="Search Course...."
          style={{borderColor:'black', borderBottomWidth:1, width: "93%", height: height *0.05}}
          value={search}
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={(value)=>setSearch(value)}
          onSubmitEditing={async (event)=>{
            await dispatch(searchCourse(event.nativeEvent.text))
          }}
        />
        <TouchableOpacity>
          <MaterialIcons name="search" size={25}/>
        </TouchableOpacity>	
      </View>
      <View style={{flex:15}}>
        <FlatList 
          data = {searchResult}
          ListEmptyComponent={<EmptyList/>}
          renderItem={({item}) => {
            return(
              <SearchResult 
                id={item.id}
                title={item.title}
                teacher={item.user.name}
                category={item.category}
                numOfStudent={item.enrolled}
                description={item.description}
                image={item.image}
                onClicked={()=>{
                  dispatch(detailCourse(item.id))
                  dispatch(contentCourse(item.id))
                  dispatch(requestCategoryCourse(item.category))
                }}
              />
            )
          }}
        />
      </View>
    </View>
)};

export default SearchPage;
