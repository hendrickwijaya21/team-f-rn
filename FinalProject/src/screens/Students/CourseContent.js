import React from 'react';
import { Text, View } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ListVideoComponent from '../../components/ListVideoComponent';

const CourseContent = ({
  route,
}) => {
  const {id, title, poster} =route.params
  const lessons = useSelector(state=>state.lesson)
  if(lessons.length == 0){
    return null
  }
  return (
    <View>
      <ListVideoComponent 
        data={lessons}
        id={id}
        title={title}
        poster={poster}
      />
    </View>
)};

export default CourseContent;
