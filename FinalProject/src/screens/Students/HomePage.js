import React, {useState, useEffect} from 'react';
import { Text, View, TextInput, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {useSelector, useDispatch} from 'react-redux';
import STYLES from '../../styles/style'
import {getUser, 
	requestAllCourse, requestCategoryCourse, 
	detailCourse, contentCourse, 
	deleteContentCourse, deleteDetailCourse, 
	getEnrolledCourse, getTestResult,
	deleteSearchCourse} from '../../store/action'
import VirtualizedView from '../../components/VirtulalizedView'

import AllCourse from '../../components/AllCourse'
import Category from '../../components/Category'
import CoursesCategory from '../../components/CoursesCategory'
import EnrollSuccessModal from './EnrollSuccessModal'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const HomePage = ({navigation}) => {
	const user = useSelector(state => state.user)
	const allCourse = useSelector(state => state.all)
	const categoryCourse = useSelector(state =>state.category)
	const [category, setCategory] = useState('')
	const CategoryList = ['Programming', 'Cooking', 'Game']
	const dispatch = useDispatch()
	useEffect(()=>{
		dispatch(getUser(user.token))
		dispatch(requestCategoryCourse(category))
		dispatch(getEnrolledCourse({token: user.token}))
		const listener = navigation.addListener('focus', () => {
			setCategory('Programming')
			dispatch(deleteSearchCourse())
			dispatch(requestAllCourse())
			dispatch(deleteDetailCourse())
			dispatch(deleteContentCourse())
			dispatch(getTestResult(user.token))

    });
    return listener
	},[])

	useEffect(()=>{
		dispatch(requestCategoryCourse(category))
	},[category])
	
	if(allCourse==[] || categoryCourse == []){
		return null
	}
  return(
    <SafeAreaView style={{flex:1}}>
			<View style={{...STYLES.header, backgroundColor: '#fff', position:'absolute', zIndex: 200}}>
				<View style={{borderBottomWidth: 4, borderColor: "#EF9C27"}}>
					<Text style={{fontSize: height * 0.02}}>LEKTUR</Text>
				</View>
				<TouchableOpacity onPress={()=>{navigation.navigate('SearchPage')}}>
					<TextInput 
						placeholder="Search Course...."
						style={{borderColor:'black', borderWidth: 1, width: width*0.5, height: height * 0.05}}
						editable={false}
					/>	
				</TouchableOpacity>
			</View>
			<VirtualizedView>
			<View style={{...STYLES.footer}}>
				<View style={{position: "absolute", width: "100%", height:height*0.4, backgroundColor:"#F0A63E", opacity:0.3}}/>
					<View style={{marginTop: height*0.1, marginBottom: height*0.04, marginLeft: 10}}>
						<Text 
							style={{fontSize:height*0.05, fontWeight:'bold'}}
						>Bring your class</Text>
						<Text
							style={{fontSize:height*0.05, fontWeight:'bold'}}
						>at home</Text>
					</View>
				<View>
					<FlatList //Search by category
						horizontal
						showsHorizontalScrollIndicator={false}
						data={allCourse}
						keyExtractor={(item)=>item.id}
						ListEmptyComponent={<View></View>}
						renderItem={({item}) => {
							return (
								<AllCourse 
									id={item.id}
									title={item.title}
									imageUrl={item.image}
									teacher={item.user.name}
									totalStudent={item.enrolled}
									onClicked={()=>{
										dispatch(detailCourse(item.id))
										dispatch(contentCourse(item.id))
										setCategory(item.category)
									}}
								/>
							)
						}}
					/>
					
				</View>
				<View style={{marginTop: height*0.08, marginBottom: height*0.03}}>
					<Text 
						style={{fontSize:height * 0.04, fontWeight:'bold'}}
					>What to learn next</Text>
				</View>
				<View>
					<Category 
						data={CategoryList}
						target={category}
						onPressed={(value) => setCategory(value)}
					/>
				</View>
				<View style={{marginBottom: height * 0.05}}>
					<FlatList 
						nestedScrollEnabled
						horizontal
						showsHorizontalScrollIndicator={false}
						data={categoryCourse}
						ListEmptyComponent={<View></View>}
						keyExtractor={(item)=>item.id}
						renderItem={({item}) => {
							return (
								<CoursesCategory 
									title={item.title}
									imageUrl={item.image}
									teacher={item.user.name}
									totalStudent={item.enrolled}
									totalVideos={item.lesson}
									description={item.description}
									category={item.category}
									totalLesson={item.material}
									id = {item.id}
									onClicked={()=>{
										dispatch(detailCourse(item.id))
										dispatch(contentCourse(item.id))
									}}s
								/>
							)
						}}
					/>
				</View>
			</View>
			<EnrollSuccessModal />
			</VirtualizedView>
    </SafeAreaView>
)};

export default HomePage;
