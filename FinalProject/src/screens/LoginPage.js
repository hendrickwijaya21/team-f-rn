import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Alert
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import {loginQuest, clearError} from '../store/action'
import STYLES from '../styles/style';

const height = Dimensions.get("screen").height

const LoginPage = ({navigation, route}) => {
  const {role} = route.params
  const [Email, setEmail] = useState('')
  const [Password, setPassword] = useState('')
  const dispatch = useDispatch();
  const unAuthorized = useSelector(state=>state.user.errorMsg)
  const [validation, setValidation] = useState({
    validPassword: true,
    validEmail:true
  })
  useEffect(() => {
    const listener = navigation.addListener('blur', () => {
      dispatch(clearError())
    });
    return listener;
  }, [navigation]);
  const handleValidPassword = (value) => {
    if(value.length >= 8 && value.length <= 32){
      setValidation({
        ...validation,
        validPassword:true
      })
    } else {
      setValidation({
        ...validation,
        validPassword:false
      })
    }
  }
  const handleValidEmail = (value) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(reg.test(value) == false){
      setValidation({
        ...validation,
        validEmail:false
      })
    } else {
      setValidation({
        ...validation,
        validEmail:true
      })
    }
  }

  const alertForNotValid = () => {
    Alert.alert(
      "Check your input",
      "There are something wrong in email or password"
    ),[
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      }
    ]
  }

  return(
    <View style={{
      flex:1,
      marginVertical:height * 0.1,
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center',
      }}>
      <Text
        style={STYLES.headerForm}>
        Welcome Back {role=='teacher'? 'Teacher' : 'Learner'}!
      </Text>
      <Text
        style={{
          fontSize: height*0.016,
          letterSpacing: 2,
          marginBottom: height*0.025,
          color: '#16213B'
        }}>
        Login to your account
      </Text>
      <Text
        style={STYLES.label}>
        Email*
      </Text>
      <TextInput style={STYLES.inputForm} 
        placeholder="john@doe.com"
        placeholderTextColor='#999'
        keyboardType="email-address"
        value={Email}
        onChangeText={(email) => {
          setEmail(email)
          handleValidEmail(email)
        }}
      /> 
      {validation.validEmail ? null : <Text style={STYLES.errorStyle}>Email is not valid</Text>}
      <Text
        style={STYLES.label}>
        Password*
      </Text>
      <TextInput style={STYLES.inputForm}
        placeholder="Password Here"
        secureTextEntry={true}
        placeholderTextColor='#999'
        value={Password}
        onChangeText={(password) =>{
          setPassword(password)
          handleValidPassword(password)
        }}
      /> 
      {validation.validPassword == true ? null : <Text style={STYLES.errorStyle}>Password have to be more than 8 and less than 32 character</Text>}
      {/* <Text style={{
        color:'#3E89AE', 
        fontSize:15}}
      >
        Forgot Password
      </Text> */}
      {unAuthorized ? <Text style={STYLES.errorStyle}>Wrong email or wrong password</Text> : null}
      <TouchableOpacity style={STYLES.buttonForm}
        onPress={()=>{
          validation.validPassword && validation.validEmail && Email.length !=0 && Password.length !=0 ?
          dispatch(loginQuest({email:Email, password: Password, role:role})) :
          alertForNotValid()
        }}
      >
        <Text style={STYLES.textForm}>
          Login
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={()=>{navigation.goBack()}}
      >
        <Text style={STYLES.authFont}
        >
          New user? Create an account
        </Text>
      </TouchableOpacity>
    </View>
    )
  }

export default LoginPage;