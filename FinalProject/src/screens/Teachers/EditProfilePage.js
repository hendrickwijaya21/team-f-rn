import React, { useState, useEffect } from 'react';
import {
  View,Text,
  TextInput,Image,
  TouchableOpacity, Alert,
  Dimensions
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import BcryptReactNative from 'bcrypt-react-native';
import { changeProfile, getEnrolledCourse } from '../../store/action';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const height = Dimensions.get("screen").height
const width = Dimensions.get("screen").width

const EditProfilePage = ({
    navigation, 
}) => {
  const user = useSelector(state => state.user)
  const [email, setEmail] = useState('')
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')
  const [image, setImage] = useState(null)
  const [isSame, setIsSame] = useState(null);
  const dispatch = useDispatch();
  useEffect(()=>{
    async function checkingPass(){
      try{
        if(password){
        setIsSame(await BcryptReactNative.compareSync(password, user.password))
        }
        if(password == ''){
          setIsSame(null)
        }
      } catch(e){
        console.log(e)
      }      
    }
    checkingPass()
  }, [password])
  const choosePhotoFromLibrary = () => {
		const options = {
			mediaType: 'photo',
      includeBase64: false,
      maxHeight: 175,
      maxWidth: 175,
    }
		launchImageLibrary(options, response => {
			if(response.uri){
				console.log(response)
				setImage({type: response.type, uri: response.uri, name: response.fileName})
			}
		})
  }
  const cameraPhoto = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 175,
      maxWidth: 175,
    }
    launchCamera(options, response => {
      if(response.uri){
        console.log(response)
				setImage({type: response.type, uri: response.uri, name: response.fileName})
      }
    })
  }
  const alertForNotValid = () => {
    Alert.alert(
      "Check your input",
      "There are something wrong in the password or there is no new input"
    ),[
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      }
    ]
  }
  return(
    <View style={{
      flex:1,
      backgroundColor:'#ffffff',
      alignItems:'center',
      justifyContent:'center'}}>
      <Image style={{
        width:175,
        height:175,
        borderRadius:100,
        marginBottom:height * 0.01}}
        source={{uri: image ? image.uri :user.image == 'https://lektur.kuyrek.com/imageUser/null' ? 'https://png.pngtree.com/png-vector/20191009/ourmid/pngtree-user-icon-png-image_1796659.jpg' : `${user.image}`}}/>
      <View style={{flexDirection: 'row', justifyContent:'space-around'}}>
        <TouchableOpacity 
          style={{ 
            marginHorizontal:width * 0.05,
            backgroundColor:'#000000',
            alignItems:'center',
            justifyContent:'center',
            borderRadius:0,
            padding:10,
            marginBottom:15}}
          onPress={() => {choosePhotoFromLibrary()}}>
          <Text style={{
            color:'#ffffff',
            fontSize:15}}>
            Browse Picture
          </Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={{
            marginHorizontal:width * 0.05,
            backgroundColor:'#000000',
            alignItems:'center',
            justifyContent:'center',
            borderRadius:0,
            padding:10,
            marginBottom:15}}
          onPress={() => {cameraPhoto()}}>
          <Text style={{
            color:'#ffffff',
            fontSize:15}}>
            Take a Picture
          </Text>
        </TouchableOpacity>
      </View>

      <TextInput style={{
        borderBottomWidth: 1,
        fontSize:15,
        color:'#000',
        padding:15,
        width:350,
        height:50,
        borderRadius:0,
        marginBottom:5}} 
        placeholder={user.name}
        placeholderTextColor='#999'
        value={name}
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={(value)=>setName(value)}
      />

      <TextInput style={{
        borderBottomWidth: 1,
        fontSize:15,
        color:'#000',
        padding:15,
        width:350,
        height:50,
        borderRadius:0,
        marginBottom:5}}
        autoCapitalize="none"
        autoCorrect={false} 
        placeholder={user.email}
        placeholderTextColor='#999'
        value={email}
        onChangeText={(value)=>setEmail(value)}
      />
      <TextInput style={{
        borderBottomWidth: 1,
        fontSize:15,
        padding:15,
        color:'#000',
        width:350,
        height:50,
        borderRadius:0,
        marginBottom:5}} 
        autoCapitalize="none"
        autoCorrect={false}
        placeholder="Confirm Password"
        secureTextEntry
        placeholderTextColor='#999'
        value={password}
        onChangeText={(value)=>setPassword(value)}
      />
      {isSame == false ? 
        <Text style={{color: "red"}}>The confirm password is not the same with current password</Text>:
        password=="" ? null :
        isSame == true ? <Text style={{color: "green"}}>The confirm password is same with current password</Text>:
        null
      }
      <TouchableOpacity style={{
        width:150, 
        height:50, 
        backgroundColor:'#16213B',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:0,
        marginTop:30,
        marginBottom:5
      }}
        onPress={async () => {
          isSame == true && (name!= ''|| email!= '' || image!=null) ?
          await dispatch(changeProfile({name: name, email:email, image:image ? image : '', token: user.token})):
          alertForNotValid()
          isSame == true && navigation.goBack()
        }}>
        <Text style={{
          color:'#ffffff',
          fontSize:15}}>
          Save
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={{
        width:150, 
        height:50, 
        backgroundColor:'#16213B',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:0,
        marginTop:30,
        marginBottom:5
        }}
        onPress={() => navigation.goBack()}>
        <Text style={{
          color:'#ffffff',
          fontSize:15}}>
          Cancel
        </Text>
      </TouchableOpacity>
                
    </View>
  )
};

export default EditProfilePage;
