import React, {useState} from 'react';
import {
  View,Text,
  TextInput, TouchableOpacity,
  Dimensions, FlatList,
} from 'react-native';
import VirtualizedView from '../../components/VirtulalizedView'
import {useSelector, useDispatch} from 'react-redux'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import STYLES from '../../styles/style';
import authAPI from '../../api/auth'

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height

const inviteStudent = async(token, course_id, email) => {
  try{
    const res = await authAPI.post(`/enrolls/invite/course/${course_id}`, {'user_email': `${email}`}, {'headers': { 'Authorization': `Bearer ${token}`}})
  } catch(e){
    console.log(e)
  }
}

const InviteStudentPage = ({navigation, route}) => {
  const {course_id} = route.params
  const listStudent = useSelector(state => state.listStudent)
  const token = useSelector(state => state.user.token)
  const [list, setList] = useState([])
  const [email, setEmail] = useState('')
  const [enrolled, setEnrolled] = useState(false)
  const [validEmail, setValidEmail] = useState(true)
  const EmptyList = () =>{
    return(
      <View style={{width:"100%", height:"100%", justifyContent:'center', alignItems:'center'}}>
        <Text style={{...STYLES.subText, fontSize:20}}>
          Invite Your Student
        </Text>
      </View>
    )
  }
  const handleValidEmail = (value) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(reg.test(value) == false){
      setValidEmail(false)
    } else {
      setValidEmail(true)
    }
    if(value ==''){
      setValidEmail(true)
    }
  }
  const checkingEnrolled = (value) => {
    for(let i = 0; i < listStudent.length; i++){
      if(listStudent[i].user.email == value){
        setEnrolled(true)
        break;
      } else{
        setEnrolled(false)
      }
    }
  }
  const deleteList = (id) => {
    let result = list.filter((list) => list.id != id)
    setList(result)

  }
  return(
    <View style={{flex:1, backgroundColor:'#fff'}}>
      <View style={{width:width*0.9, margin:width * 0.05}}>
        <View style={{flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
          <TextInput 
            style={{...STYLES.inputForm, marginBottom:0}} 
            placeholder="Type student email"
            placeholderTextColor='#696969'
            keyboardType="email-address"
            value={email}
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={(newValue)=>{
              handleValidEmail(newValue)
              checkingEnrolled(newValue)
              setEmail(newValue)
            }}
          />
          <TouchableOpacity
            disabled={!validEmail || enrolled ? true : false}
            onPress={()=>{
              if(email != ''){
                setList([...list, {"id": list.length + 1, "email": email}])
                setEmail('')
              }
            }}
          >
            <MaterialIcons name="add" style={{fontSize: height * 0.04, marginRight:width*0.04}}/>
          </TouchableOpacity>
        </View>
        {!validEmail && <Text style={{...STYLES.errorStyle,alignSelf:'center'}}>Not a valid email</Text>}
      </View>
      <VirtualizedView>
      <View style={{
        margin:width * 0.1,
        height:height * 0.4,
        borderWidth:1,
        borderRadius:12,
        overflow: 'hidden',
      }}>
        <FlatList
          data={list}
          ListEmptyComponent={<EmptyList/>}
          keyExtractor={item=>item.id.toString()}
          renderItem={({item})=>{
            return(
              <View style={{flexDirection:'row', justifyContent:'space-between', margin: 10, padding: 10, borderBottomWidth: 1, borderColor:"#e9e9e9"}}>
                <Text style={STYLES.emailList}>
                  {item.email}
                </Text>
                <TouchableOpacity
                  onPress={()=>{
                    deleteList(item.id)
                  }}
                >
                  <MaterialIcons name="cancel" size={24} />
                </TouchableOpacity>
              </View>
            )
          }}
        />
        
      </View>
      </VirtualizedView>
      
      <TouchableOpacity style={{...STYLES.buttonForm, width: width*0.8, alignSelf:'center'}}
        onPress={async () => {
          for(let i = 0; i < list.length; i++){
            await inviteStudent(token, course_id, list[i].email)
          }
          setList([]);
        }}>
        <Text style={STYLES.textWhite}>
          Invite
        </Text>
      </TouchableOpacity>
    </View>
  )
}

export default InviteStudentPage;
