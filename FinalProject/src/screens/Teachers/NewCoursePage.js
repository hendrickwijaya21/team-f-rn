import React, {useState} from 'react';
import { Text, View, ScrollView, TouchableOpacity, Dimensions, TextInput } from 'react-native';

//Component
import SubNavBar from '../../components/subNavBar'

//Page NavBar
import ListStudentPage from './ListStudentPage'
import AddCoursePage from './AddCoursePage'
import AddAssessmentPage from './AddAssessmentPage';

const width = Math.round(Dimensions.get("screen").width)
const height = Dimensions.get("screen").height


const NewCoursePage = ({
    params,
}) => {
	const [subMenu, setSubMenu] = useState('Course');
	const [header, setHeader] = useState([{id:0,title:'', overview:''}])

	const handleHeaderValue = (index, key, value) => {
		const updatedArray = header.map(arr => {
			if(index == arr['id']){
				arr[key] = value
			}
			return arr
		})
		setHeader(updatedArray)
	}
	console.log(header)
	return (
    <>
			<SubNavBar
				navBar ={['Course', 'Assignment', 'Students']}
				target = {subMenu}
				onPressed={(value)=>setSubMenu(value)}
			/>
			{subMenu == "Course" && <AddCoursePage dataHeader = {header} onChange={handleHeaderValue}/>}
			{subMenu == "Assignment" && <AddAssessmentPage/>}
			{subMenu == "Students" && <ListStudentPage/>}
    </>
)};

export default NewCoursePage;
