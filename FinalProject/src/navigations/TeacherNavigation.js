import 'react-native-gesture-handler';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import DashBoardPage from '../screens/Teachers/DashboardPage'
import InviteStudentPage from '../screens/Teachers/InviteStudentPage'
import NewCoursePage from '../screens/Teachers/NewCoursePage'
import TeacherProfilePage from '../screens/Teachers/TeacherProfilePage'
import EditProfilePage from '../screens/Teachers/EditProfilePage'
import ListStudentPage from '../screens/Teachers/ListStudentPage'

const CompleteDashBoard = () => {
  const Stack = createStackNavigator()
  return(
    <Stack.Navigator>
      <Stack.Screen 
        name="DashBoard"
        component={DashBoardPage}
        options={{title: "Course", headerTitleAlign:"center"}}
      />
      <Stack.Screen 
        name="NewCoursePage" 
        component={NewCoursePage}
        options={{title: "New Course", headerTitleAlign:"center"}}
      />
      <Stack.Screen 
        name="InviteStudent"
        component={InviteStudentPage}
        options={{title: "Invite Student", headerTitleAlign:"center"}}
      />
      <Stack.Screen 
        name="ListStudent"
        component={ListStudentPage}
        options={{title: "List Student", headerTitleAlign:"center"}}
      />
    </Stack.Navigator>
  )
}

const TeacherProfile = () => {
  const Stack = createStackNavigator()
  return (
    <Stack.Navigator>
      <Stack.Screen 
        name="ProfilePage"
        component={TeacherProfilePage}
        options={{headerShown: false}}
      />
      <Stack.Screen 
        name="EditProfile" 
        component={EditProfilePage}
        options={{title: "Edit Profile", headerTitleAlign:"center"}}
      />
    </Stack.Navigator>
  )
}

const TeacherNavigation = () => {
  const Tab = createBottomTabNavigator();
  return(
    <Tab.Navigator headerMode="none"
      tabBarOptions={{
        activeTintColor: "#000",
        labelStyle:{
          fontSize: 12,
          fontWeight:"bold"
        }
      }}
    >
      <Tab.Screen 
        name ="CompleteDashBoard" 
        component = {CompleteDashBoard}
        options = {{
          tabBarIcon: ({ focused}) => (
            <MaterialIcons name="library-books" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
          ),
          tabBarLabel: "Dashboard"
        }}
      />
      <Tab.Screen 
        name="TeacherProfile" 
        component = {TeacherProfile}
        options = {{
          tabBarIcon: ({ focused}) => (
            <MaterialIcons name="person" color={focused? "#EF9C27" : "#999999"} size={focused? 35: 25} />
          ),
          tabBarLabel: "Profile"
        }}
        
      />
    </Tab.Navigator>
)};

export default TeacherNavigation;
