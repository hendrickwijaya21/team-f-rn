import TeacherCourses from '../models/teacher-courses'

const TeachCourse = [
  new TeacherCourses(
    '1',
    'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    'React Native',
    12,
    12,
    123
  ),
  new TeacherCourses(
    '2',
    'https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    'Node Js',
    12,
    12,
    123
  ),new TeacherCourses(
    '3',
    'https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    'Front End',
    12,
    12,
    123
  )
]

export default TeachCourse