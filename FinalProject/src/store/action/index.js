import ACTION from '../types'

export const registerQuest = (payload) => {
  return {type: ACTION.SIGNUP_REQUESTED, payload : payload}
}
export const loginQuest = (payload) => {
  return {type: ACTION.SIGNIN_REQUESTED, payload: payload}
}
export const getUser = (payload) => {
  return {type: ACTION.GET_USER_REQUEST, payload: payload}
}
export const signOut = () => {
  return {type: ACTION.SIGN_OUT}
}
export const checkStorage = () => {
  return {type: ACTION.CHECK_STORAGE}
}
export const clearError = () => {
  return {type: ACTION.CLEAR_ERROR}
}
export const clearStatus = () => {
  return {type: ACTION.CLEAR_STATUS}
}
export const successEnroll = (payload) => {
  return {type: ACTION.ENROLL_SUCCESS, payload: payload}
}
export const clearEnroll = () => {
  return {type: ACTION.CLEAR_MODAL}
}
export const requestAllCourse = () => {
  return {type: ACTION.GET_ALL_COURSE}
}
export const requestCategoryCourse = (payload) => {
  return {type: ACTION.GET_CATEGORY_COURSE, payload: payload}
}
export const detailCourse = (payload) => {
  return {type: ACTION.GET_DETAIL_COURSE, payload: payload}
}
export const contentCourse = (payload) => {
  return {type: ACTION.GET_CONTENT_COURSE, payload:payload}
}
export const deleteDetailCourse = () => {
  return {type: ACTION.DELETE_DETAIL_COURSE}
}
export const deleteContentCourse = () => {
  return {type: ACTION.DELETE_CONTENT_COURSE}
}
export const getCourseByTeacher = (payload) => {
  return {type: ACTION.GET_COURSE_TEACHER, payload: payload}
}
export const getListStudent = (payload) => {
  return {type: ACTION.GET_LIST_STUDENT, payload:payload}
}
export const deleteListStudent = () => {
  return {type: ACTION.DELELTE_LIST_STUDENT}
}
export const enrollClass = (payload) => {
  return {type: ACTION.ENROLL_CLASS, payload: payload}
}
export const getEnrolledCourse = (payload) => {
  return {type: ACTION.GET_ENROLLED_COURSE, payload: payload}
}
export const getLessonsCourse = (payload) => {
  return {type: ACTION.GET_LESSONS_COURSE, payload: payload}
}
export const deleteLessonCourse = () => {
  return {type: ACTION.DELETE_LESSONS_COURSE}
}
export const getTestResult= (payload) => {
  return {type: ACTION.GET_TEST_RESULT, payload:payload}
}
export const changeProfile = (payload) => {
  return {type: ACTION.REQUEST_CHANGE_PROFILE, payload: payload}
}
export const searchCourse = (payload) => {
  return {type: ACTION.SEARCH_COURSE, payload:payload}
}
export const deleteSearchCourse = () => {
  return {type: ACTION.DELETE_SEARCH_COURSE}
}