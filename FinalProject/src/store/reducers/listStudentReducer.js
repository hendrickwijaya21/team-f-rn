import ACTION from '../types'

const initialState = []

export default function listStudentReducer (state = initialState, action) {
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_LIST_STUDENT:
      return payload
    case ACTION.DELELTE_LIST_STUDENT:
      return []
    default:
      return state
  }
}