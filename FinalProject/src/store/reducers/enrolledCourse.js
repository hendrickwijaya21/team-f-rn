import ACTION from '../types'

const initialState = []

export default function enrolledCourse (state=initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_ENROLLED_COURSE:
      return payload
    default:
      return state
  }
}