import ACTION from '../types'

const initialState = []

export default function contentReducer (state = initialState, action) {
  const {type, payload} = action
  switch(type){
    case ACTION.DELETE_CONTENT_COURSE:
      return []
    case ACTION.PUT_CONTENT_COURSE:
      return payload
    default:
      return state
  }
}