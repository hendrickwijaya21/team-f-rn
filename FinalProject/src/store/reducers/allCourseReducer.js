import ACTION from '../types'

const initialState=[]
export default function allCourseReducer(state = initialState, action){
  const {type, payload} = action
  switch (type){
    case ACTION.PUT_ALL_COURSE:
      return payload
    default:
      return state
  }
}