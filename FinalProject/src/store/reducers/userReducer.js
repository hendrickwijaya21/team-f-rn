import ACTION from '../types'

const initialState={
  token:'',
  role:'',
  errorMsg: '',
  status:'',
  loading:'false',
}

export default function userReducer(state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.LOADING:
      return {
        ...state,
        loading: true
      }
    case ACTION.SIGNUP_SUCCESS:
      return {
        ...state,
        status:payload,
        errorMsg: '',
        loading:false
      }
    case ACTION.SIGNIN_SUCCESS:
      return {
        token: payload.token,
        role: payload.role,
        errorMsg: '',
        status:'',
        loading:false,
      }
    case ACTION.GET_USER_SUCCESS:
      return {
        ...state, ...payload
      }
    case ACTION.SIGN_OUT:
      return {
        token:'',
        role:'',
        errorMsg: '',
        status:'',
        loading: false
      }
    case ACTION.ERROR:
      return {
        ...state,
        errorMsg: payload,
        loading:false
      }
    case ACTION.CLEAR_ERROR:
      return {
        ...state,
        errorMsg:''
      }
    case ACTION.PUT_CHANGE_PROFILE:
      return {
        ...state, ...payload
      }
    case ACTION.CLEAR_STATUS:
      return {
        ...state, status:''
      }
    default:
      return state
  }
}