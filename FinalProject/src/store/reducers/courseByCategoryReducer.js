import ACTION from '../types'

const initialState=[]

export default function courseByCategoryReducer(state = initialState, action){
  const {type, payload} = action
  switch(type){
    case ACTION.PUT_CATEGORY_COURSE:
      return payload
    default:
      return state
  }
}