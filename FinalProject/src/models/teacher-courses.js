class TeacherCourses {
  constructor(id, imageUrl, titleCourse, totalVideos, totalLesson, totalStudent){
    this.id = id;
    this.imageUrl = imageUrl;
    this.titleCourse = titleCourse;
    this.totalVideos = totalVideos;
    this.totalLesson = totalLesson;
    this.totalStudent = totalStudent
  }
}

export default TeacherCourses