class CourseDetails {
  constructor(imageUrl, title, teacher, category, totalVideos, totalLesson, description, contents) {
    this.imageUrl= imageUrl;
    this.title = title;
    this.teacher = teacher;
    this.category = category;
    this.totalVideos = totalVideos;
    this.totalLesson = totalLesson;
    this.description = description;
    this.contents = contents;
  }
}
export default CourseDetails